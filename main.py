import json
import turtle
import urllib.request
import time
import webbrowser
import geocoder

url = "http://api.open-notify.org/astros.json"
response = urllib.request.urlopen(url)
result = json.loads(response.read())
file = open("iss.txt", "w")
file.write("There are currently" + str(result["number"]) + " astronauts on the ISS: \n\n")
people = result["people"]
for p in people:
    file.write(p['name'] + " - on board" + "\n")
# print longitute and latitude
g = geocoder.ip('me')
file.write("\nYour current lat / long is: " + str(g.latlng))
file.close()
webbrowser.open("iss.txt")

# setting up world map in turtle module
screen = turtle.Screen()
screen.setup(7000, 3500)
screen.setworldcoordinates(-180, -90, 180, 90)

# Load world map image
screen.bgpic("map.gif")
screen.register_shape("iss.gif")
iss = turtle.Turtle()
# iss is the shape of iss gif
iss.shape("iss.gif")
# give angle while moving
iss.setheading(45)
iss.penup()

# Track ISS in real time
while True:
    # load current status of ISS using second API
    url = "http://api.open-notify.org/iss-now.json"
    response = urllib.request.urlopen(url)
    result = json.loads(response.read())

    # Extract the ISS Location
    location = result["iss_position"]
    lat = location['latitude']
    lon = location['longitude']

    # Output Long and Lat to terminal
    lat = float(lat)
    lon = float(lon)
    print("\nLatitude: " + str(lat))
    print("\nLongitude: " + str(lon))

    # update ISS location on map
    iss.goto(lon, lat)

    # refresh each 5 seconds
    time.sleep(5)
